#!/usr/bin/env python3

import pymongo

class NodeBBDatabase:

    def _get_children(self, tree, node):
        children = {node}
        if node in tree.keys():
            for i in tree[node]:
                children = children | self._get_children(tree, i)

        return children

    def get_category_ancestors(self, cat_id):
        tree = dict()
        for i in self.collection.find({"_key": {"$regex": r"^category:\d*$"}},
                                      {"_id": False, "cid": True, "parentCid": True}):
            tree.setdefault(int(i['parentCid']), set()).add(i['cid'])

        return self._get_children(tree, cat_id)

    def get_users(self):
        return {int(i['uid']): i['username'] for i in self.collection.find({"_key": {"$regex": r"^user:\d*$"}},
                                                                      {"_id": False, "uid": True, "username": True})}

    def get_posts(self, date_start, date_end, categories):
        posts = list(self.collection.find({"_key": {"$regex": r"^post:\d*$"},
                                           "timestamp": {"$gte": date_start.timestamp() * 1000,
                                                         "$lt": date_end.timestamp() * 1000}},
                                          {"_id": False, "pid": True, "tid": True, "uid": True}))
        posts = [{'pid': int(p['pid']), 'tid': int(p['tid']), 'uid': int(p['uid'])} for p in posts]
        tids = {t['tid'] for t in posts}
        tids = {(t['tid'], int(t['cid'])) for t in self.collection.find({"_key": {"$regex": r"^topic:\d*$"},
                                                        "tid": {"$in": list(tids)}},
                                                       {"_id": False, "cid": True, "tid": True})}
        tids = {t[0] for t in tids if t[1] in categories}
        posts = [x for x in posts if x['tid'] in tids]
        post_dict = dict()
        for p in posts:
            post_dict[p['uid']] = post_dict[p['uid']] + 1 if p['uid'] in post_dict.keys() else 1

        return post_dict

    def __init__(self, host, port, user, password, database):
        self.db = pymongo.MongoClient(host, port, username=user, password=password, authSource=database)[database]
        self.collection = self.db.objects
