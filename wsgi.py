#!/usr/bin/env python3
from urllib import parse
import datetime
import ism_app
import ism


def router(environ, start_response):
    start_response('200 OK', [('Content-Type', 'application/json; charset=utf-8')])

    params = parse.parse_qs(environ['QUERY_STRING'])
    if 'data' not in params.keys():
        return [ism.ISMStats.get_error_json("BRAK_DATY").encode('utf-8')]

    try:
        date = datetime.datetime.strptime(params['data'][0], "%Y-%m-%d")
    except ValueError:
        return [ism.ISMStats.get_error_json("BLEDNA_DATA").encode('utf-8')]

    category = params['kategoria'][0] if'kategoria' in params.keys() else 0

    try:
        category = int(category)
    except ValueError:
        return [ism.ISMStats.get_error_json("BLEDNA_KATEGORIA").encode('utf-8')]

    return [ism_app.get_ism_data(date, category).encode('utf-8')]


if __name__ == '__main__':
    try:
        from wsgiref.simple_server import make_server
        httpd = make_server('', 8080, router)
        print('Serving on port 8080...')
        httpd.serve_forever()
    except KeyboardInterrupt:
        print('Goodbye.')