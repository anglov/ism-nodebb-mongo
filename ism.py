#!/usr/bin/env python3

import datetime
import json


class ISMStats:
    version = "p0.1"

    def get_json(self, date, category):
        res = {
            'wersja': ISMStats.version,
            'status': "SUKCES"
        }
        dt = datetime.datetime(date.year, date.month, date.day)
        categories = self.db.get_category_ancestors(category)
        users = self.db.get_users()
        posts = self.db.get_posts(dt, dt + datetime.timedelta(days=1), categories)
        res['wynik'] = [{'u': k, 'n': users[k] if k in users.keys() else k, 'c': v} for k, v in posts.items()]
        return json.dumps(res)

    @staticmethod
    def get_error_json(err):
        return json.dumps({
            'wersja': ISMStats.version,
            'status': "BLAD",
            'wynik': err
        })

    def __init__(self, d):
        self.db = d
