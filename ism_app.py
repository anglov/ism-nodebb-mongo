#!/usr/bin/env python3

import argparse
import configparser
import sys
import datetime
import nodebb_db
import ism


def get_ism_data(date, category):
    config = configparser.ConfigParser(allow_no_value=True)
    config.read(sys.path[0] + '/config.ini')
    d = nodebb_db.NodeBBDatabase(config.get('db', 'host'),
                                 int(config.get('db', 'port')),
                                 config.get('db', 'user'),
                                 config.get('db', 'password'),
                                 config.get('db', 'database'))
    i = ism.ISMStats(d)
    return i.get_json(date, category)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("date", type=datetime.date.fromisoformat, help="Chose a day for statistics")
    parser.add_argument("category", type=int, help="Chose a forum category for statistics", default=0)
    args = parser.parse_args()
    print(get_ism_data(args.date, args.category))


